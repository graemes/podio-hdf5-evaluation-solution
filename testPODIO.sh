#!/usr/bin/env bash
#
# This is a script that executes the necessary commands to setup and
# test PODIO for the GSoC evaluation test

# Bail on error
set -e

# Checkout PODIO
git clone https://github.com/AIDASoft/podio.git
cd podio

# Make sure environment is correct
source /opt/root/bin/thisroot.sh
source ./init.sh

# Create build and install areas
mkdir build install
cd build

# Compile and run the tests
cmake -DCMAKE_INSTALL_PREFIX=../install ..
make -j4 install
make test

echo "Successfully built PODIO and ran all unit tests"
