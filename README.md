# Implementation of an HDF5 IO layer for PODIO
## Google Summer of Code 2019 Evaluation Test - Model Answer

## Prerequisites

This solution will work in the Ubuntu18 container environment,
`graemeastewart/u18-podio`.

## Checkout and Compile PODIO tests

- Change directory to a scratch area
- Run the shell script `testPODIO.sh`

## Read and Write an HDF5 representation of SimpleStruct PODIO data

The code for this is stored in the `src` subdirectory
    - `createH5dataset.cpp` - code to create HDF5 file
    - `readH5dataset.cpp` - code to reread HDF5 file
    - `simplestruct.h` - C++ for SimpleStruct class
    - `h5simplestruct.h` - HDF5 compound DataType for SimpleStruct

The header files are used to avoid code duplication between the
reader and the writer implementations.

### Compilation and Running Instructions

- Use the `CMakeLists.txt` file in the `src` directory:

```sh
cd /tmp
cmake <PATH_TO_SOLUTION>/src
make
./createH5dataset
./readH5dataset
```
