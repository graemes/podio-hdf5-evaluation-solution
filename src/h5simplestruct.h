// Define the SimpleStuct representation for HDF5

#include <iostream>

#include "H5Cpp.h"
#include "simplestruct.h"

#ifndef H5_SIMPLESTRUCT_H
#define H5_SIMPLESTRUCT_H

class H5_simplestruct {
private:
  H5::CompType h5_datatype;
  hsize_t pdims[1];
  hid_t p_t;

public:
  H5_simplestruct();

  auto h5dt() {
    return h5_datatype;
  }
};

H5_simplestruct::H5_simplestruct() {
  h5_datatype = H5::CompType(sizeof(SimpleStruct));
  pdims[0] = {4};
  p_t = H5Tarray_create2(H5T_NATIVE_INT, 1, pdims);
  h5_datatype.insertMember( "x", HOFFSET(SimpleStruct, x), H5::PredType::NATIVE_INT);
  h5_datatype.insertMember( "y", HOFFSET(SimpleStruct, y), H5::PredType::NATIVE_INT);
  h5_datatype.insertMember( "z", HOFFSET(SimpleStruct, z), H5::PredType::NATIVE_INT);
  h5_datatype.insertMember( "p", HOFFSET(SimpleStruct, p), p_t);
}

#endif
