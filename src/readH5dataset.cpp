#include <iostream>
#include <string>
#include <array>
#include <cassert>

#include "H5Cpp.h"
#include "simplestruct.h"
#include "h5simplestruct.h"

using namespace H5;

const H5std_string	FILE_NAME("podioH5test.h5");
const H5std_string	DATASET_NAME("SimpleStruct");
const int events=100;

int main (void)
{
  // Create our data block
  SimpleStruct eData[events];

  // Try block to detect exceptions raised by any of the H5 calls
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately
    // Exception::dontPrint();

    // Open file and dataset
    H5File file(FILE_NAME, H5F_ACC_RDONLY);
    DataSet dataset(file.openDataSet(DATASET_NAME));

    // HDF5 representation of the SimpleStruct
    H5_simplestruct h5ss;

    // Read dataset
    dataset.read(eData, h5ss.h5dt());


    // Check we got back the expected values
    for (int i=0; i<events; ++i){
      assert(eData[i].x == i);
      assert(eData[i].y == i);
      assert(eData[i].z == i);
      for (int j=0; j<4; ++j) {
        assert(eData[i].p[j] == i*i + j);
      }
    }
  }  // end of try block

  // catch failure caused by the H5File operations
  catch(FileIException error)
  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSet operations
  catch(DataSetIException error)
  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSpace operations
  catch(DataSpaceIException error)
  {
    error.printErrorStack();
    return -1;
  }

  std::cout << "SimpleStruct data was read back and verified" << std::endl;
  return 0;  // successfully terminated
}
