#include <iostream>
#include <string>
#include <array>

#include "H5Cpp.h"
#include "simplestruct.h"
#include "h5simplestruct.h"

using namespace H5;

const H5std_string	FILE_NAME("podioH5test.h5");
const H5std_string	DATASET_NAME("SimpleStruct");
const int events=100;

int main (void)
{
  // Create our data
  SimpleStruct eData[events];

  // Fill with some predictable values
  for (int i=0; i<events; ++i){
    eData[i].x = i;
    eData[i].y = i;
    eData[i].z = i;
    for (int j=0; j<4; ++j) {
      eData[i].p[j] = i*i + j;
    }
  }

  // Try block to detect exceptions raised by any of the H5 calls
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately
    Exception::dontPrint();

    // Create a new file using the default property lists.
    H5File file(FILE_NAME, H5F_ACC_TRUNC);

    // Create the data space for the dataset.
    hsize_t dims[1];               // dataset dimensions
    dims[0] = events;
    DataSpace dataspace(1, dims);

    // HDF5 representation of the SimpleStruct
    H5_simplestruct h5ss;

    // Create the dataset.
    DataSet dataset = file.createDataSet(DATASET_NAME, h5ss.h5dt(), dataspace);
    dataset.write(eData, h5ss.h5dt());

  }  // end of try block

  // catch failure caused by the H5File operations
  catch(FileIException error)
  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSet operations
  catch(DataSetIException error)
  {
    error.printErrorStack();
    return -1;
  }

  // catch failure caused by the DataSpace operations
  catch(DataSpaceIException error)
  {
    error.printErrorStack();
    return -1;
  }

  std::cout << "SimpleStruct data was created and written" << std::endl;
  return 0;  // successfully terminated
}
